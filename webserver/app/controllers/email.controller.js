/** main
 * @file paket.controller
 * @author Konrad Bode (kon.bode.21@lehre.mosbach.dhbw.de)
 * @brief Controller for the pakets
 * @version 2.0
 * @date 2023-03-04
 * 
 * @copyright Copyright (c) 2023
 * 
*/


const db = require("../models");
const Email = db.email;

const mqtt = require("../helper/Mqtt");
var sock = mqtt.instance;

/** create
 * 
 * @brief
 * function create a new Email and safe it in the Database
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @param   [out]   message     StatusMessage
 * @see create
 * 
 */

exports.create = (req, res) => {
    // Validate request
    for (const key in req.body){
        if (!req.body[key]) {
            res.status(400).send({
            message: "Content can not be empty!"
        });
        return
        }
    }
    // Create a Email
    const email = {
        adress: req.body.adress
    };

    // Sending Mqtt Message to PI for Welcome Mail
    const msg = {
        function : "email",
        value : "welcome"
    };


    //sending new Email over MQTT to the PI
    sock.publish("runtime/email", JSON.stringify(msg));

    // Save Email in the database
    Email.create(email)
        .then(data => {
        res.send(data);
        })
        .catch(err => {
            res.status(500).send({
            message:
            err.message || "Some error occurred while inserting the email."
        });
    });
};

/** findAll
 * 
 * @brief
 * function to find all Emails
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @param   [out]   message     StatusMessage
 * @see findAll
 * 
 */
exports.findAll = (req, res) => {

    Email.findAll()
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
            message:
            err.message || "Some error occurred while retrieving email."
        });
    });
};

/** delete
 * 
 * @brief
 * function to delete a given Email from the Database
 * @param   [in]    req
 * @param   [out]   res         StatusCode
 * @param   [out]   message     StatusMessage
 * @see delete
 * 
 */
exports.delete = (req, res) => {

    const adress = req.params.adress;

    Email.destroy({where: { adress: adress }})
    .then(num => {
        if (num == 1) {
            res.send({
                message: "Email was deleted successfully!"
            });
        } else {
            res.send({
                message: `Cannot delete Email ${adress}. Maybe Email was not found!`
            });
        }
    })
    .catch(err => {
        res.status(500).send({
            message: "Could not delete Email " + adress
        });
    });
};

/** update
 * 
 * @brief
 * function to change a existing Email
 * @param   [in]    req
 * @param   [out]   res         StatusCode
 * @param   [out]   message     StatusMessage
 * @see update
 * 
 */
exports.update = (req, res) => {

    const adress = req.params.adress;
  
    Email.update({adress: adress, new: false}, {
      where: { adress: adress }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Email was updated successfully."
          });
        } else {
          res.status(404).send({
            message: `Cannot update Email ${adress}. Maybe Email was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating Email " + adress
        });
      });
  
  }