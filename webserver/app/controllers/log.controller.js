/** main
 * @file paket.controller
 * @author Konrad Bode (kon.bode.21@lehre.mosbach.dhbw.de)
 * @brief Controller for the pakets
 * @version 2.0
 * @date 2023-03-04
 * 
 * @copyright Copyright (c) 2023
 * 
*/


const db = require("../models");
const Log = db.logs;

/** create
 * 
 * @brief
 * the function create a new Log and safe it in the Database
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @see create
 * 
 */
exports.create = (req, res) => {

  // Validate request

  for (const key in req.body){
    if (!req.body[key]) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return
    }
  }

  console.log("New Log: " + req.body.msg)
  
  // Create a Log
  const log = {
    text: req.body.msg.substr(6),
    code: req.body.msg.substr(0,3),
  };

  // Save Log in the database
  Log.create(log)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Log."
      });
  });
};


/** findAll
 * 
 * @brief
 * the function get all Logs
 * @param   [in]    req
 * Qparam   [out]   res  StatusCode
 * @param   [out]   Logs
 * @see findAll
 * 
 */
exports.findAll = (req, res) => {

  Log.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving logs."
      });
    });
};