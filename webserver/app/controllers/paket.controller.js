/** main
 * @file paket.controller
 * @author Konrad Bode (kon.bode.21@lehre.mosbach.dhbw.de)
 * @brief Controller for the pakets
 * @version 2.0
 * @date 2023-03-04
 * 
 * @copyright Copyright (c) 2023
 * 
*/

const db = require("../models");
const Paket = db.pakete;

/** create
 * 
 * @brief
 * the function create a new Paket in the Database
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @see create
 * 
 */
exports.create = (req, res) => {

    // Validate request
    for (const key in req.body){
      if (!req.body[key]) {
        res.status(400).send({
          message: "Content can not be empty!"
        });
        return
      }
    }

    // Create a Paket Json
    const paket = {
      barcode: req.body.barcode,
      name: req.body.name,
      kategorie: req.body.kategorie,
      lieferant: req.body.lieferant,
      verkaeufer: req.body.verkaeufer
    };

    //saving the Paket in the Database
    Paket.create(paket)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the paket."
        });
    });
};


/** accept
 * 
 * @brief
 * the function controlls the accaption of the Pakets
 * @param   [in]   req
 * @param   [out]  res  StatusCode and Date
 * @see accept
 * 
 */
exports.accept = (req, res) => {

  const barcode = req.params.barcode

  Paket.findOne({ where: { barcode: barcode} })
    .then(paket => {
      if(paket.angenommen == null){
        paket.angenommen = Date.now();
        console.log(paket.id)
        Paket.update({ angenommen: Date.now()}, {
          where: { id: paket.id }
        })
        .then(() => {
          //202: accepted
          res.status(202).send()
        })
        .catch(() =>{
          //500: internal server Error
          res.status(500).send()
        })
      } else {
        //403: forbidden
        res.status(403).send()
      }
    })
    .catch(() => {
      //404: not found
      res.status(404).send()
    });

}

/** findAll
 * 
 * @brief
 * the function show all stored Pakets
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @see findAll
 * 
 */
exports.findAll = (req, res) => {

    Paket.findAll()
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving pakete."
        });
      });
};


/** findOne
 * 
 * @brief
 * the function return the Paket with the given id
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @see findOne
 * 
 */
exports.findOne = (req, res) => {
  
  const id = req.params.id;

  Paket.findByPk(id)
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({
          message: `Cannot find Paket with id=${id}.`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Paket with id=" + id
      });
    });
};



/** update
 * 
 * @brief
 * this function Accepting the Barcode
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @see update
 * 
 */

exports.update = (req, res) => {

  const id = req.params.id;

  Paket.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Paket was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Paket with id=${id}. Maybe Paket was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Paket with id=" + id
      });
    });

}

/** delete
 * 
 * @brief
 * this function will delete the given paket
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @see delete
 * 
 */
exports.delete = (req, res) => {

    const id = req.params.id;
  
    Paket.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Paket was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete Paket with id=${id}. Maybe Paket was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete Paket with id=" + id
        });
      });
};