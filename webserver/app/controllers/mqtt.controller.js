/** main
 * @file paket.controller
 * @author Jannik Erb (jan.erb.21@lehre.mosbach.dhbw.de)
 * @brief Controller for the pakets
 * @version 2.0
 * @date 2023-03-04
 * 
 * @copyright Copyright (c) 2023
 * 
*/

const mqtt = require("../helper/Mqtt");
var sock = mqtt.instance;

/** open
 * 
 * @brief
 * the function to open the box
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @see open
 */
exports.open = (req,res) => {
    msg = {
        function : "lock",
        value : true
    }
    sock.publish("runtime/io/action", JSON.stringify(msg));
    res.send();
}

/** close
 * 
 * @brief
 * the function to close the box
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @see     open
 * @info    it is possible to switch the lock off while it's open, but it's not used
 */
exports.close = (req,res) => {
    msg = {
        function : "lock",
        value : false
    }
    sock.publish("runtime/io/action", JSON.stringify(msg));
    res.send();
}

/** lighton
 * 
 * @brief
 * function to switch the path light on
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @see     lighton
 */
exports.lighton = (req,res) => {
    msg = {
        function : "light",
        value : true
    }
    sock.publish("runtime/io/action", JSON.stringify(msg));

    res.send();
}

/** lightoff
 * 
 * @brief
 * function to switch the path light off
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @see     lightoff
 */
exports.lightoff = (req,res) => {
    msg = {
        function : "light",
        value : false
    }
    sock.publish("runtime/io/action", JSON.stringify(msg));
    res.send();
}

/** doorEnable
 * 
 * @brief
 * function to enable the Door opener Button
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @see     doorEnable
 */
exports.doorEnable = (req,res) => {
    msg = {
        function : "doorEnable",
        value : true
    }
    sock.publish("runtime/io/action", JSON.stringify(msg));
    res.send();
}

/** doorDisable
 * 
 * @brief
 * function to disable the Door opener Button
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @see     doorDisable
 */
exports.doorDisable = (req,res) => {
    msg = {
        function : "doorEnable",
        value : false
    }
    sock.publish("runtime/io/action", JSON.stringify(msg));
    res.send();
}

/** ringEnable
 * 
 * @brief
 * function to enable the Door Bell
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @see     ringEnable
 */
exports.ringEnable = (req,res) => {
    msg = {
        function : "ringEnable",
        value : true
    }
    sock.publish("runtime/io/action", JSON.stringify(msg));
    res.send();
}

/** ringDisable
 * 
 * @brief
 * function to disable the Door Bell
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @see     ringDisable
 */
exports.ringDisable = (req,res) => {
    msg = {
        function : "ringEnable",
        value : false
    }
    sock.publish("runtime/io/action", JSON.stringify(msg));
    res.send();
}


/** setTime
 * 
 * @brief
 * function to controll the Function Time of the System
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @see     setTime
 */
exports.setTimes = (req, res) => {
    console.log(req.body);
    times = {
        start: req.body.start,
        stop: req.body.stop
    }
    msg = {
        function: "time",
        value: times
    }
    sock.publish("runtime/io/time", JSON.stringify(msg));
    res.send();
}

/** functionRun
 * 
 * @brief
 * function to set the System to Run
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @see     functionRun
 */
exports.functionRun = (req, res) => {
    console.log(req.body);
    msg = {
        function : "function",
        value : true
    }
    console.log("funciton restart")
    sock.publish("runtime/io/function", JSON.stringify(msg));
    res.send();
}

/** getData
 * 
 * @brief
 * function to ask for the asking the act IO States
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @see     getData
 */
exports.getData  = (req, res) => {
    sock.publish("runtime/io/get", "true");
    res.send();
}

/** getFunction
 * 
 * @brief
 * function to ask for the asking the act System Function
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @see     getFunction
 */
exports.getFunction = (req, res) => {
    //sock.publish("runtime/io/get", "true");
    res.send();

}

/** getTimes
 * 
 * @brief
 * function to ask for the asking the act System Run Time
 * @param   [in]   req
 * @param   [out]  res  StatusCode
 * @see     getTimes
 */
exports.getTimes = (req, res) => {
    sock.publish("runtime/io/get", "true");
    res.send()
}