/** 
 * @class MQTTClient
 * @author  Jannik Erb (jan.erb.21@lehre.mosbach.dhbw.de)
 * @brief   MQTTClient for connecting the IO's with the PI
 * @version 2.0
 * @date  2023-03-11
 * 
 * @copyright Copyright (c) 2023
 * 
*/

var MQTTClient = (function(){

    var instance;
    const option = require('../config/mqtt.config');

   /** Constructor
   * 
   * @brief Constructor as Singelton
   * @param[in] none
   * @param[out]  none
   * @returns client
   * @see mqttClass()
   */
    function MqttClass() {
        const mqtt = require('mqtt');
        client = mqtt.connect(option.HOST, option.LOGIN);
        client.on('connect', function(){
          console.log("mqtt connected");
          client.subscribe("runtime/io/rm");
          client.subscribe("esp/outside");
        })
        
        return client
    }

    return {
      /** getInstance
       * 
       * @brief return the instance of the Class
       * @param[in] none
       * @param[out]  none
       * @returns instance
       * @see getInstance
       */
      getInstance: function(){
        if (!instance) {
          console.log("mqtt initalisiert");
          instance = new MqttClass();
        }
        return instance;
      }
  }})();


  //export the "access"
  var instance = MQTTClient.getInstance();
  exports.instance = instance;