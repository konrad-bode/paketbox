    /** Paket.create
   * @file  log.model
   * @brief
   * defines paket database table
   * @param   [in]  sequelize Instance
   * @param   [out] none
   * @returns Log
   * @see Log
   */

module.exports = (sequelize, Sequelize) => {
  const Log = sequelize.define("log", {
    id: {
      field: "id",
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      primaryKey: true,
    },
    text: {
      field: "text",
      type: Sequelize.STRING,
      allowNull: false
    },
    code: {
      field: "code",
      type: Sequelize.INTEGER,
      allowNull: false
    },
  },{
    timestamps: true,
    tableName: "Logs"
  });

  return Log;
};