    /** eamil.model
   * @file  email.model
   * @brief
   * defines email database table
   * @param   [in]  sequelize Instance
   * @param   [out] none
   * @returns Log
   * @see Email
   */

module.exports = (sequelize, Sequelize) => {
    const Email = sequelize.define("email", {
        adress: {
            field: "adress",
            type: Sequelize.STRING,
            allowNull: false,
            primaryKey: true
        },
        new: {
            field: "new",
            type: Sequelize.BOOLEAN,
            defaultValue: true,
            allowNull: false,
        }
    },{
        timestamps: false,
        tableName: "email"
    });
    
    return Email;
};