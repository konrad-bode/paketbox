    /** index
   * @file  index
   * @brief
   * Database Handler
   * @param   [in]  none
   * @param   [out] none
   * @see index
   */

const dbConfig = require("../config/db.config.js");

//creating Sequelize Instance
const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: '0',
  timezone: '+01:00',

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.pakete = require("./paket.model.js")(sequelize, Sequelize);
db.logs = require("./log.model.js")(sequelize, Sequelize);
db.email = require("./email.model.js")(sequelize, Sequelize);

module.exports = db;