    /** Paket.create
   * @file  paket.model
   * @brief
   * defines paket database table
   * @param   [in]  sequelize Instance
   * @param   [out] none
   * @returns Paket
   * @see Paket
   */

module.exports = (sequelize, Sequelize) => {
    const Paket = sequelize.define("paket", {
      id: {
        field: "id",
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
      },
      barcode: {
        field: "barcode",
        type: Sequelize.STRING,
        allowNull: false,
      },
      angenommen: {
        field: "angenommen",
        type: Sequelize.DATE,
        defaultValue: null
      },
      kategorie: {
        field: "kategorie",
        type: Sequelize.STRING,
        allowNull: false
      },
      name: {
        field: "name",
        type: Sequelize.STRING,
        allowNull: false
      },
      lieferant: {
        field: "lieferant",
        type: Sequelize.STRING,
        allowNull: false
      },
      verkaeufer: {
        field: "verkaeufer",
        type: Sequelize.STRING,
        allowNull: false
      }
    },{
      tableName: "pakete"
    });
  
    return Paket;
  };