/** 
 * @file paket.routers.js
 * @author  Konrad Bode (kon.bode@lehre.mosbach.dhbw.de)
 * @brief   API adresses for controlling the Paket functions
 * @version 2.0
 * @date  2023-03-11
 * 
 * @copyright Copyright (c) 2023
 * 
*/

module.exports = app => {
    const pakete = require("../controllers/paket.controller.js");
  
    var router = require("express").Router();
    
    router.get("/", pakete.findAll);
    
    router.get("/:id", pakete.findOne);

    router.put("/:id", pakete.update);

    router.put("/accept/:barcode", pakete.accept)

    router.post("/", pakete.create);

    router.delete("/:id", pakete.delete);
  
    app.use('/api/pakete', router);
  };