/** 
 * @file paket.routers.js
 * @author  Jannik Erb (jan.erb.12@lehre,mosbach.dhbw.de)
 * @brief   API adresses for controlling the IO functions
 * @version 2.0
 * @date  2023-03-11
 * 
 * @copyright Copyright (c) 2023
 * 
*/

module.exports = app => {
    
    const mqtt = require("../controllers/mqtt.controller.js");

    var router = require("express").Router();

    router.post("/lock/open", mqtt.open);

    router.post("/lock/close", mqtt.close);

    router.post("/light/on", mqtt.lighton);

    router.post("/light/off", mqtt.lightoff);

    router.post("/ring/enable", mqtt.ringEnable);

    router.post("/ring/disable", mqtt.ringDisable);

    router.post("/door/enable", mqtt.doorEnable);

    router.post("/door/disable", mqtt.doorDisable);

    router.post("/io/getdata", mqtt.getData);

    router.post("/function/run", mqtt.functionRun)

    router.post("/times", mqtt.setTimes);

    app.use('/api/mqtt', router);

};