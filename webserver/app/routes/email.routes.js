/** 
 * @file paket.routers.js
 * @author  Konrad Bode (kon.bode@lehre.mosbach.dhbw.de)
 * @brief   API adresses for controlling the Email functions
 * @version 2.0
 * @date  2023-03-11
 * 
 * @copyright Copyright (c) 2023
 * 
*/


module.exports = app => {
    const email = require("../controllers/email.controller.js");

    var router = require("express").Router();

    router.get("/", email.findAll);

    router.post("/", email.create);

    router.delete("/:adress", email.delete);

    router.put("/:adress", email.update);

    app.use('/api/email', router);

};