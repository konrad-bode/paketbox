/** 
 * @file paket.routers.js
 * @author  Konrad Bode (kon.bode.21@lehre.mosbach.dhbw.de)
 * @brief   API adresses for controlling the Log functions
 * @version 2.0
 * @date  2023-03-11
 * 
 * @copyright Copyright (c) 2023
 * 
*/


module.exports = app => {
    const logs = require("../controllers/log.controller.js");
  
    var router = require("express").Router();
  
    router.get("/", logs.findAll);

    router.post("/", logs.create);

    app.use('/api/logs', router);

  };