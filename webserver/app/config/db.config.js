module.exports = {
  HOST: "localhost",
  USER: "node",
  PASSWORD: "passwort123",
  DB: "paketbox",
  dialect: "mysql",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};
