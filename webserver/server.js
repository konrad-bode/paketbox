/** 
 * @file server.js
 * @author  Konrad Bode (kon.bode@lehre.mosbach.dhbw.de)
 * @brief   Webserver main
 * @version 2.0
 * @date  2023-03-11
 * 
 * @copyright Copyright (c) 2023
 * 
*/

// require
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const http = require("http");
const websocket = require("ws");
const app = express();
const mqtt = require('./app/helper/Mqtt');
const mqttController = require('./app/controllers/mqtt.controller');
const config = require('./app/config/ws.config');






//create Webserver
const server = http.createServer(app);
const websocketServer = new websocket.Server({server : server});



//when a websocket connection is established
websocketServer.on('connection', socket => {
  socket.on('message', message => {
  },);
});



//start the web server
server.listen(config.WebSocketPort, () => {
  console.log("Websocket server started on port " + config.WebSocketPort);
});




function updateClient (message){
  websocketServer.clients.forEach((client) => {

    client.send(message);
  });
}



var mqttClient = mqtt.instance;


mqttClient.on('message', function(topic, message, packet){
  console.log(`${message}`);
  updateClient(`${message}`);
})


const path = __dirname + config.viewPath;

app.use(express.static(path));
app.use(cors());

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));




const db = require("./app/models");
const { send } = require("process");
const { WebSocketServer } = require("ws");
const { json } = require("body-parser");
const { measureMemory } = require("vm");


db.sequelize.sync()
  .then(() => {
    console.log("Synced db.");
  })
  .catch((err) => {
    console.log("Failed to sync db: " + err.message);
  });




// website 
app.get('/', function (req,res) {
  res.sendFile(path + "index.html");
});




// REST API 
require("./app/routes/paket.routes")(app);
require("./app/routes/log.routes")(app);
require("./app/routes/email.routes")(app);
require("./app/routes/mqtt.routes")(app);

// set port, listen for requests
const PORT = process.env.PORT || config.Port;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});