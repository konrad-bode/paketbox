#!/bin/bash

mkdir -p /etc/paketbox

cd ..
echo "$PWD"

ln -s $PWD/cpp/main /etc/paketbox/scanner
ln -s $PWD/python/main.py /etc/paketbox/main
ln -s $PWD/webserver/server.js /etc/paketbox/server

cp -a $PWD/services/. /etc/systemd/system

sudo chown root /etc/systemd/system/webserver.service
sudo chown root /etc/systemd/system/paketboxPy.service
sudo chown root /etc/systemd/system/scanner.service

sudo systemctl enable webserver.service
sudo systemctl enable paketboxPy.service
sudo systemctl enable scanner.service