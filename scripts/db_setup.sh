#!/bin/bash

# Farben
RED='\033[0;31m'
ORANGE='\033[0;33m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

printf "MariaDB (MySQL) node-user und paketbox Datenbank anlegen...\n"
sudo mysql -u root --password="123456" < ./db.sql
[ $? -eq 0 ] && printf "${GREEN}OK${NC}\n" || printf "${RED}FEHLER${NC}\n"