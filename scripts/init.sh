#!/bin/bash

# Farben
GREEN='\033[0;32m'
NC='\033[0m' # No Color

printf "Installationen ... \n"
./install.sh
printf "${GREEN}OK${NC}\n"

printf "Db Benutzer anlegen ... \n"
./db_setup.sh
printf "${GREEN}OK${NC}\n"

printf "Systemd Dienste anlegen ...\n"
./createSystemd.sh
printf "${GREEN}OK${NC}\n"

printf "${GREEN}Alles Fertig${NC}\n"