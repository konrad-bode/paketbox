#!/bin/bash

# Farben
RED='\033[0;31m'
ORANGE='\033[0;33m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

# Generell keine Abfragen
export DEBIAN_FRONTEND=noninteractive

# Abkürzungen
INSTALL="sudo -E apt install -y"
UPDATE="sudo -E apt update -qq"
UPGRADE="sudo -E apt upgrade -fuy"

STEPS="8"

printf "${ORANGE}[1/${STEPS}]${NC} Paketlisten aktualisieren...\n"
$UPDATE
[ $? -eq 0 ] && printf "${GREEN}OK${NC}\n" || printf "${RED}FEHLER${NC}\n"

printf "${ORANGE}[2/${STEPS}]${NC} Aktualisierungen installieren...\n"
$UPGRADE
[ $? -eq 0 ] && printf "${GREEN}OK${NC}\n" || printf "${RED}FEHLER${NC}\n"

printf "${ORANGE}[3/${STEPS}]${NC} MariaDB (MySQL) installieren...\n"
$INSTALL mariadb-server
[ $? -eq 0 ] && printf "${GREEN}OK${NC}\n" || printf "${RED}FEHLER${NC}\n"

printf "${ORANGE}[4/${STEPS}]${NC} MQTT-Broker (Mosquitto) installieren...\n"
$INSTALL mosquitto mosquitto-clients
[ $? -eq 0 ] && printf "${GREEN}OK${NC}\n" || printf "${RED}FEHLER${NC}\n"

printf "${ORANGE}[5/${STEPS}]${NC} NodeJS (und npm) installieren...\n"
$INSTALL nodejs npm
[ $? -eq 0 ] && printf "${GREEN}OK${NC}\n" || printf "${RED}FEHLER${NC}\n"

printf "${ORANGE}[6/${STEPS}]${NC} Python Paketmanager (pip) und GPIO-Bibliothek installieren...\n"
$INSTALL python3-pip python3-libgpiod

printf "${ORANGE}[7/${STEPS}]${NC} Python Bibliotheken installieren...\n"
pip install -r pipInstall

printf "${ORANGE}[8/${STEPS}]${NC} Node Bibliotheken installieren...\n"
npm --prefix ../webserver install ../webserver

[ $? -eq 0 ] && printf "${GREEN}OK${NC}\n" || printf "${RED}FEHLER${NC}\n"