#!/bin/bash

sudo systemctl enable mosquitto.service

echo "listener 1883" >> etc/mosquitto/config.d
echo "allow_anonymous false" >> etc/mosquitto/config.d

sudo mosquitto_passwd -c /etc/mosquitto/passwd py < "mosquitto_password.txt"

echo "password_file /etc/mosquitto/passwd"

sudo systemctl restart mosquitto.service