/** main
 * @file main.cpp
 * @author  Jannik Erb (jan.erb.21@lehre.mosbach.dhbw.de) <br>
 * 			Jonas Bodi (jon.bodi.21@lehre.mosbach.dhbw.de)
 * @brief 	Function for getting Keyboard Interrupt from the Scanner.
 * @version 1.0
 * @date 2023-03-11
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include "barcoderead.cpp"

int main(int argc, char* argv[])
{	
	if( argc != 2 )
	{
		cout << "Usage: " << argv[0] << " <devicename> " << endl;
		return -1;
	}	
	
	BarcodeReader reader( argv[ 1 ] );
	
	if( !reader.isReady() )
	{
		cout << "Could not open barcode reader." << endl;
		return -1;
	}
	
	// Read barcodes until Ctrl+C
	while (1) 
	{
		//cout << reader.readCode() << endl;
		std::string barcode = reader.readCode();
		//cout << barcode << endl;

    	std::string msg2 = "mosquitto_pub --topic runtime/scanner  -u py -P 'mosquitto' -p 1883  -m '" + barcode + "'";
		cout << msg2 << endl;
    	system(msg2.c_str());
	}

	return 0;
}