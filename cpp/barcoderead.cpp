/*
 * File:        barcoderead.cpp
 * Description: Tries to read keyboard events from barcode reader HID (Linux only) and sends every code read to standard output
 * Author/date: Joerg Mielebacher, 09/Sep/2017
 * Based on:    https://github.com/jeffpiazza/read-barcode/blob/master/barcode.c
 * Build:       g++ barcoderead.cpp -o barcoderead
 * Usage:       barcoderead <devicename>
 * Example:     ./barcoderead /dev/input/by-id/usb-13ba_Barcode_Reader-event-kbd
 */ 

#include <iostream>
#include <string>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/input.h>

using namespace std;

// Simple barcode reader class that reads from keyboard event file
class BarcodeReader
{
public:
    // Create new barcode reader by providing device name 
	BarcodeReader( const string& devicepath="" ); 
	
	// Initialize barcode reader (usually called from constructor)
	bool   initialize( const string& devicepath );
	
	// Check if reader can be used
	bool   isReady() const;
	
	// Read next barcode (until Enter)
	string readCode();

	// Close barcode reader
	~BarcodeReader();
	
private:
    bool isready; // Reader can be used
	int  device;  // File descriptor 
};

BarcodeReader::BarcodeReader( const string& devicepath )
{
	initialize( devicepath );
}

bool BarcodeReader::initialize( const string& devicepath )
{
	isready = false;
	
    device = open( devicepath.c_str(), O_RDONLY );
  
	if( device == -1 )
		return isready;

// Check for exclusive lock	
//	if( !ioctl( device, EVIOCGRAB, 1 ) )
//		return isready;

	isready = true;
	
	return isready;
}

bool BarcodeReader::isReady() const
{
	return isready;
}

string BarcodeReader::readCode() 
{
	string code;
	
	// Reader not ready;
	if( !isReady() )
		return code;
	
	bool shift = false; // Shift button is pressed
    char ch    = 0;     // Current character code

	while( 1 )
	{
		struct  input_event ev; // Structure for input event
        ssize_t cnt = read( device, &ev, sizeof( ev ) );

		// Check for error (no bytes read from file)
        if( cnt <= 0 )
		{
			if( errno == EINTR )
				continue;
			
			return "";
		}			
		
		// Check if correct number of bytes was read
		if( cnt != sizeof( ev ) )
			return "";
		
		// Skip non-key events (e.g. EV_SYN, EV_MSC)
		if (ev.type != EV_KEY) 
			continue;
				
		// Check, if shift is pressed
		if( ( ev.value == 1 ||  // Key pressed
		      ev.value == 2 )   // Key autorepeat
		   && ev.code == KEY_LEFTSHIFT )
		{
			shift = true; // Enable shift
			continue;
		}
		
		// Skip anything else than key released
		if( ev.value != 0 ) 
			continue;
		
		// Read key code
		std::cout<<ev.code<< std::endl;
		switch (ev.code) 
		{
			case KEY_LEFTSHIFT: shift = false; continue; // Disable shift
			case KEY_0: ch = '0'; break;
			case KEY_1: ch = '1'; break;
			case KEY_2: ch = '2'; break;
			case KEY_3: ch = '3'; break;
			case KEY_4: ch = '4'; break;
			case KEY_5: ch = '5'; break;
			case KEY_6: ch = '6'; break;
			case KEY_7: ch = '7'; break;
			case KEY_8: ch = '8'; break;
			case KEY_9: ch = '9'; break;

			case KEY_A: ch = shift ? 'A' : 'a'; break;
			case KEY_B: ch = shift ? 'B' : 'b'; break;
			case KEY_C: ch = shift ? 'C' : 'c'; break;
			case KEY_D: ch = shift ? 'D' : 'd'; break;
			case KEY_E: ch = shift ? 'E' : 'e'; break;
			case KEY_F: ch = shift ? 'F' : 'f'; break;
			case KEY_G: ch = shift ? 'G' : 'g'; break;
			case KEY_H: ch = shift ? 'H' : 'h'; break;
			case KEY_I: ch = shift ? 'I' : 'i'; break;
			case KEY_J: ch = shift ? 'J' : 'j'; break;
			case KEY_K: ch = shift ? 'K' : 'k'; break;
			case KEY_L: ch = shift ? 'L' : 'l'; break;
			case KEY_M: ch = shift ? 'M' : 'm'; break;
			case KEY_N: ch = shift ? 'N' : 'n'; break;
			case KEY_O: ch = shift ? 'O' : 'o'; break;
			case KEY_P: ch = shift ? 'P' : 'p'; break;
			case KEY_Q: ch = shift ? 'Q' : 'q'; break;
			case KEY_R: ch = shift ? 'R' : 'r'; break;
			case KEY_S: ch = shift ? 'S' : 's'; break;
			case KEY_T: ch = shift ? 'T' : 't'; break;
			case KEY_U: ch = shift ? 'U' : 'u'; break;
			case KEY_V: ch = shift ? 'V' : 'v'; break;
			case KEY_W: ch = shift ? 'W' : 'w'; break;
			case KEY_X: ch = shift ? 'X' : 'x'; break;
			case KEY_Y: ch = shift ? 'Y' : 'y'; break;
			case KEY_Z: ch = shift ? 'Z' : 'z'; break;

			case KEY_MINUS: ch = '-'; break;
			case KEY_EQUAL: ch = '='; break;
			case KEY_SEMICOLON: ch = ';'; break;
			case KEY_APOSTROPHE: ch = '\''; break;
			case KEY_GRAVE: ch = '`'; break;
			case KEY_BACKSLASH: ch = '\\';
			case KEY_COMMA: ch = ','; break;
			case KEY_DOT: ch = '.'; break;
			case KEY_SLASH: ch = '/'; break;
			case KEY_SPACE: ch = ' '; break;
			case KEY_KPASTERISK: ch = '*'; break;

			case KEY_ENTER:
			default:	
				return code; // Code scanned
		}
		
		// Append current key to code
		code += ch;
	}
	
	// Never reached
	return "";
}

BarcodeReader::~BarcodeReader()
{
	close( device );
}




