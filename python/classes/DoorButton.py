##
#   @class Button
#   @brief  class for controlling the Door Button <br>
#           (Button on the Entrance to open Door)
#   @author     Jannik Erb(jan.erb.21@lehre.mosbach.dhbw.de)
#   @version 1.1
#   @date 2023-03-07

import gpiod
from classes.Config import Config
from classes.Email import Email
from classes.DoorLock import DoorLock
import threading
import time


class DoorButton:
    
    __doorButton = None
    __con = None
    __except = None
    __doorLock = None
    _instance = None

    ## constructor
    #   @brief Constructor as Singelton <br> init the DoorButton Pin
    #   @param [in]    none
    #   @param [out]   __dorrButton    GPIO Door Button
    #   @param [out]   __mqttClient    MqttClient
    #   @param [out]   __con           Config
    #   @return _instance
    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(DoorButton, cls).__new__(cls)
            cls.__con = Config()
            cls.__doorLock = DoorLock()
            DOORPIN = cls.__con.getConfigAsInt("DoorButtonPin", 27)    #getting GpioPin from config

            chip = gpiod.chip('gpiochip0')

            cls.__doorButton = chip.get_line(DOORPIN)

            reqconfig = gpiod.line_request()
            reqconfig.consumer = "doorbutton"
            reqconfig.flag = gpiod.line_request.FLAG_BIAS_PULL_DOWN
            reqconfig.request_type = gpiod.line_request.DIRECTION_INPUT

            cls.__doorButton.request(reqconfig)

        return cls._instance


    ## __getDoorButton
    #   @brief  function to read the State of the Button
    #   @param  [in]    none
    #   @param  [out]   none
    #   @return int GPIO.get_value (1 = true, 0 = false)
    def __getDoorButton(self):
        return self.__doorButton.get_value()
    
    ## __doorButtonThread
    #   @brief  thread for checkin if button is pressed
    #   @param  [in]    none
    #   @param  [out]   none
    def __doorButtonThread(self):
        while(True):
            if self.__getDoorButton():
                #self.__except.log(206) #exception 
                
                self.__doorLock.open()
                time.sleep(1)
            else:
                #nothing
                pass
            time.sleep(0.1)

    ## run
    #   @brief  start thread
    #   @param  [in]    none
    #   @param  [out]   none    
    def run(self):
        thread = threading.Thread(target = self.__doorButtonThread)
        thread.daemon = True
        thread.start()

