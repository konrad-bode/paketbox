#Gruppe32#
##
#   @class usTime
#   @brief  class for control the Enable/Disable Time of the box
#   @author Jannik Erb (jan.erb.21@lehre.mosbach.dhbw.de)
#   @version 1.1
#   @date 2023-03-07

from classes.MqttClient import MqttClient
from classes.Config import Config
import time
from datetime import datetime

class UseTime:

    __mqttClient = None
    __mqttTopic = "runtime/io/rm"
    __config = None
    __stopTime = None
    __run = None
    __startTime = "08:00"
    __stopTime = "20:00"
    _instance = None


    ## constructor
    #   @brief  Constructor as Singelton
    #   @param  [in]    none
    #   @param  [out]   __config        Config
    #   @param  [out]   __mqttClient    MqttClient
    #   @param  [out]   __run           True
    #   @return   _instance
    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super(UseTime, cls).__new__(cls, *args, **kwargs)
            cls.__config = Config()
            cls.__mqttClient = MqttClient()
            cls.__run = True

        return cls._instance
    
    ## __dayTime
    #   @brief  function check if the currentTime is in the Client timerange
    #   @param  [in]    currentTime
    #   @param  [out]   __run       True if in Timerang, False if outside
    #   @info   funciton if the startTime < stopTime (DayMode)
    def __dayTime(self, currentTime):

        if currentTime < self.__startTime or currentTime > self.__stopTime:
            self.__run = False
        elif currentTime >= self.__startTime or currentTime <= self.__stopTime:
            self.__run = True
        
    ## __dayTime
    #   @brief  function check if the currentTime is in the Client timerange
    #   @param  [in]    currentTime
    #   @param  [out]   __run       True if in Timerang, False if outside
    #   @info   funciton if the startTime > stopTime (NightMode)
    def __nightTime(self, currentTime):
        if currentTime > self.__startTime and currentTime < self.__stopTime:
            self.__run = False
        elif currentTime <= self.__startTime and currentTime >= self.__stopTime:
            self.__run = True

    ## setTime
    #   @brief  function change the Time over the Webpage
    #   @param  [in]    currentTime
    #   @param  [out]   __startTime     Timestamp
    #   @param  [out]   __stopTime      Timestamp
    #   @info   Config file will be overwritten with new values
    def setTime(self, startTime, stopTime):
        self.__startTime = datetime.timestamp(datetime.strptime(startTime, '%H:%M'))
        self.__stopTime = datetime.timestamp(datetime.strptime(stopTime, '%H:%M'))
        self.__config.updateConfig("TurnOnTime", startTime)
        self.__config.updateConfig("TurnOffTime", stopTime)

    ## getTime
    #   @brief  function for getting Time falues from config and return to WebClient
    #   @param  [in]    currentTime
    #   @param  [out]   __startTime     Timestamp
    #   @param  [out]   __stopTime      Timestamp
    def getTime(self):
        startTime = self.__config.getConfigAsString("TurnOnTime", "00:00")
        stopTime = self.__config.getConfigAsString("TurnOffTime", "00:00")

        #convering string to time tuple
        self.__startTime = datetime.timestamp(datetime.strptime(startTime, '%H:%M'))
        self.__stopTime = datetime.timestamp(datetime.strptime(stopTime, '%H:%M'))
        message = '{"function": "time", "value":{"start":"'+ startTime +'", "stop":"'+ stopTime +'"}}'
        self.__mqttClient.mqttPublish(self.__mqttTopic, message, 0)


    ## __periodOfUse
    #   @brief  function distribute the times
    #   @param  [in]    none
    #   @param  [out]   none
    def __periodOfUse(self):
        now = time.strftime('%H:%M')
        currentTime = datetime.timestamp(datetime.strptime(now, '%H:%M'))

        if self.__startTime < self.__stopTime:
            self.__dayTime(currentTime)
        elif self.__startTime > self.__stopTime:
            self.__nightTime(currentTime)
        else:
            #log wrong values
            pass


    ## check
    #   @brief  function for checking if in timeRange
    #   @param  [in]    none
    #   @param  [out]   none
    #   @return __run   (True if in Timerange)
    def check(self):
        self.__periodOfUse()
        return self.__run