#Gruppe32#
##
#   @class DoorLock
#   @brief  class the MQTT connection
#   @author     Jonas Bodi (jon.bodi.21@lehre.mosbach.dhbw.de)
#               Jannik Erb (jan.erb.21@lehre.mosbach.dhbw.de)
#   @version 1.1
#   @date 2023-03-07

import gpiod
import time
import threading
from classes.Config import Config
from classes.MqttClient import MqttClient

class DoorLock:
    
    __door = None
    __mqttClient = None
    __con = None
    __mqttTopic = "runtime/io/rm"
    __ioOpen = '{"function": "doorLock", "value": true}'  #enable
    __ioClose = '{"function": "doorLock", "value": false}'  #enable
    __ioEnable = '{"function": "doorEnable", "value": true}'  #enable
    __ioDisable = '{"function": "doorEnable", "value": false}'  #disable
    __ioState = False
    __doorEnable = True
    __threadrunning = False


    _instance = None

    ## constructor
    #   @brief Constructor as Singelton <br> init Door Lock
    #   @param [in]    none
    #   @param [out]   __door          GPIO Door Lock
    #   @param [out]   __mqttClient    MqttClient
    #   @param [out]   __con           Config
    #   @return _instance
    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(DoorLock, cls).__new__(cls)
            cls.__mqttClient = MqttClient()
            cls.__con = Config()
            DOORPIN = cls.__con.getConfigAsInt("DoorPin", 19)    #getting GpioPin from config
            chip = gpiod.chip('gpiochip0')

            cls.__door = chip.get_line(DOORPIN)

            reqconfig = gpiod.line_request()
            reqconfig.consumer = "doorlock"
            reqconfig.flag = gpiod.line_request.FLAG_ACTIVE_LOW
            reqconfig.request_type = gpiod.line_request.DIRECTION_OUTPUT

            cls.__door.request(reqconfig)

        return cls._instance

        ## __openthread
    #   @brief  function to open the box as thread
    #   @param  [in]    none
    #   @param  [out]   __ioState switch the light

    def __openthread(self): #calls open method from Locker, waits for 5 seconds and calls close method from Locker
        self.__threadrunning = True
        self.__door.set_value(1)
        self.__ioState = True
        self.__mqttClient.mqttPublish(self.__mqttTopic, self.__ioOpen, 0)
        time.sleep(5)
        self.__door.set_value(0)
        self.__ioState = False
        self.__mqttClient.mqttPublish(self.__mqttTopic, self.__ioClose, 0)
        self.__threadrunning = False

    ## open
    #   @brief  function to open the box and aktivate the thread __openthread
    #   @param  [in]    none
    #   @para   [out]   none
    def open(self):  #calls openthread 
        
        #only start thread if not used
        if not self.__threadrunning and self.__doorEnable:
            thread = threading.Thread(target = self.__openthread)
            thread.daemon = True
            thread.start()
        else:
            #idle
            pass

    ## enable
    #   @brief  function to enable the Lock for the Button
    #   @param  [in]    none
    #   @para   [out]   __doorEnable set to True
    def enable(self):
        self.__doorEnable = True
        self.__mqttClient.mqttPublish(self.__mqttTopic, self.__ioEnable, 0)

    ## disable
    #   @brief  function to disbale the Lock for the Button
    #   @param  [in]    none
    #   @para   [out]   __doorEnable set to False
    def disable(self):
        self.__doorEnable = False
        self.__mqttClient.mqttPublish(self.__mqttTopic, self.__ioDisable, 0)

    ## getenableState
    #   @brief  function to return the state of the DoorLock <br> via mqtt
    #   @param  [in]    none
    #   @para   [out]   none
    def getenableState(self):   #sends io state to webserver

        if self.__doorEnable:
            self.__mqttClient.mqttPublish(self.__mqttTopic, self.__ioEnable, 0)
        else:
            self.__mqttClient.mqttPublish(self.__mqttTopic, self.__ioDisable, 0)
        