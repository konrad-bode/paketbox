#Gruppe32#
##
#   @class Email
#   @brief  class for sending Emails
#   @author     Jannik Erb (jan.erb.21@lehre.mosbach.dhbw.de)
#   @version 2.0
#   @date 2023.03.11

import smtplib
import ssl
import os
import json
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from classes.Api import Api
from classes.MqttClient import MqttClient
from classes.Exceptionhandler import ExceptionHandler
class Email:

    __mqttClient = None
    __ex = None
    __smtp = None
    __api = None
    __host = "smtp.gmail.com"
    __email = "derpaketbox@gmail.com"
    __pwd = "zdevsqczgxnzxshr"
    __mqttTopic = "runtime/email"
    __smtpPort = 465
    __abpath =  os.path.dirname(os.path.realpath(__file__)) + "/mail/"
    __resemails = []

    #if the message file unter the directory /mail unable to open, the message will send with the __htmldefault string
    __htmldefault = "<html><p>one or more file were deleted unter path: " + __abpath + ". <br> You can download these files in the documentation <br> You still get all Infomation with the mail. <br> {failure} <br> {message} </p></html>"
    __htmlInfoHtml = ""
    __htmlWelchomeHtml = ""
    __htmlErrorHtml = ""
    

    _instance = None

    ## constructor
    #   @brief Constructor as Singelton <br> init Door Lock
    #   @param [in]    none
    #   @param [out]   __api                Api
    #   @param [out]   __mqttClient         MqttClient
    #   @param [out]   __ex                 ExceptionHandler
    #   @param [out]   __htmlErrorHtml      Email Design
    #   @param [out]   __htmlInforHtml      Email Design
    #   @param [out]   __htmlWelcomeHtml    Email Design
    #   @param [out]   __smtp               smtplib
    #   @return _instance
    def __new__(cls, *args, **kwargs):
        if cls._instance is None:

            cls._instance = super(Email, cls).__new__(cls, *args, **kwargs)
            cls.__api = Api()
            cls.__mqttClient = MqttClient()
            cls.__ex = ExceptionHandler()
            cls.__htmlErrorHtml = cls.__geterrhtml(cls)
            cls.__htmlWelchomeHtml = cls.__getwelcomehtml(cls)
            cls.__htmlInfoHtml = cls.__getinfohtml(cls)
            context = ssl.create_default_context()
            cls.__smtp = smtplib.SMTP_SSL(cls.__host, cls.__smtpPort, context=context)
            cls.__connect(cls)
            #cls.__getEmail(cls)
            cls.__mqttClient.mqttSubscribe(cls.__mqttTopic, 0)
        else:
            #instance already exists
            pass

        return cls._instance

    ##  destructor
    #   @brief  Destructor 
    #   @param  [in]    none
    #   @param  [out]   none
    def __delattr__(self) -> None:
        self.__smtp.close()

    ## __geterrhtml
    #   @brief  function to read the errhtml from
    #   @param  [in]    none
    #   @param  [out]   none
    #   @return html
    def __geterrhtml(self):
        html = ""
        try:
            file = open(self.__abpath + "errormail.html")
            for line in file:
                html += line
        except:
            html = self.__htmldefault

        return html

    ## __getwelcomehtml
    #   @brief  function to read the welcomehtml from
    #   @param  [in]    none
    #   @param  [out]   none
    #   @return html
    def __getwelcomehtml(self):
        html = ""
        try:
            file = open(self.__abpath + "welcomemail.html")
            for line in file:
                html += line
        except:
            html = self.__htmldefault

        return html

    ## __infohtml
    #   @brief  function to read the infohtml from
    #   @param  [in]    none
    #   @param  [out]   none
    #   @return html
    def __getinfohtml(self):
        html = ""
        try:
            file = open(self.__abpath + "infomail.html")
            for line in file:
                html += line
        except:
            html = self.__htmldefault

        return html

    ## __connect
    #   @brief  function for connection to smtp client
    #   @param  [in]    none
    #   @param  [out]   none
    #   @return None    
    def __connect(self):
        try:
            self.__smtp.login(self.__email, self.__pwd)
        except Exception as ex:
            self.__ex.log(402)
            return None
        
        self.__ex.log(205)

    ## __sendmail
    #   @brief  function for sending a mail with a text
    #   @param  [in]    html            html Argument
    #   @param  [in]    send_message    Message to send
    #   @param  [out]   none
    #   @return None  
    def __sendmail(self, html, send_message):
    
        counter = 0
        for mail in self.__resemails:

            if counter >= 5:
                break
            else:
                counter += 1
            
            if mail != None:
                #lot's of devices use this to show the current email
                send_message["To"] = mail
                mes_part_html = MIMEText(html, "html")
                send_message.attach(mes_part_html)
                self.__smtp.sendmail(self.__email, mail, send_message.as_string())
                self.__ex.log(201)
            else:
                self.__ex.log(300)
                #no mail defined
                pass

    ## __getEmail
    #   @brief  function for getting all Email Adresses on the Api and sending Welcome Mail
    #   @param  [in]    none
    #   @param  [out]   none
    def __getEmail(self):
        text = self.__api.get("email")
        if text == None:
            self.__ex.log(401)
            #log server not running
            return None
        else:
            #nothing
            pass
        j_val = json.loads(text)
        i = 0
        
        while i < 5:
            addr = None
            new = None
            try:
                addr = j_val[i]['adress']
                new = j_val[i]['new']

                self.__resemails.append(addr)

            except Exception as ex:
                #end of field
                break
            
            finally:
                if new and addr != None:
                    self.__sendWelcomeEmail(addr)
                else:
                    pass

            i += 1

    ## __sendWelcomeEmail
    #   @brief  function for sending the welcome Email
    #   @param  [in]    mail    Mailadress
    #   @param  [out]   none
    def __sendWelcomeEmail(self, mail):
        send_message = MIMEMultipart("alternative")
        send_message["Subject"] = "Hello here is your automatic Paket System"
        send_message["From"] = self.__email
        #lot's of devices use this to show the current email
        send_message["To"] = mail

        mes_part_html = MIMEText(self.__htmlWelchomeHtml, "html")
        send_message.attach(mes_part_html)

        try:
            #not with the send funvtion because the function will send it to all emails
            self.__smtp.sendmail(self.__email, mail, send_message.as_string())
        except Exception as ex:
            self.__ex.log(402)
            pass

        self.__api.put("email", mail)

    ## __sendErrorEmail
    #   @brief  function for sending the error Email
    #   @param  [in]    message         failure Message
    #   @param  [in]    failureCode     failure Code
    #   @param  [out]   none
    def sendErrorEmail(self, message, failureCode):

        send_message = MIMEMultipart("alternative")
        send_message["Subject"] = "Error " + str(failureCode)
        send_message["From"] = self.__email

        html = ""
        try:
            html = self.__htmlErrorHtml.format(message = message, failure = failureCode)
        except:
            html += self.__htmlErrorHtml + message

        try:
            self.__sendmail(html, send_message)
        except Exception as ex:
            self.__ex.log(402)
            pass


    ## __sendInfoEmail
    #   @brief  function for sending the info Email
    #   @param  [in]    message         info Message
    #   @param  [in]    failureCode     info Code
    #   @param  [out]   none
    def sendInfoEmail(self, message, failureCode):

        send_message = MIMEMultipart("alternative")
        send_message["Subject"] = "Info " + str(failureCode)
        send_message["From"] = self.__email
        html = ""
        try:
            html = self.__htmlInfoHtml.format(message = message, failure = failureCode)
        except:
            html += self.__htmlInfoHtml + message

        try:
            self.__sendmail(html, send_message)
        except Exception as ex:
            self.__ex.log(402)
            pass


    ## __sendInfoEmail
    #   @brief  function for checking of new elements of the webpage
    #   @param  [in]    none
    #   @param  [out]   none
    def infoMailCheck(self):
        msg =  self.__mqttClient.getdata(self.__mqttTopic, 0, True)
        if msg != None:
            self.__getEmail()


