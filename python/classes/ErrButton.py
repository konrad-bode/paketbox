#Gruppe 32
##
#   @class ErrButton
#   @brief  class for controlling the Error Button <br>
#           (Button on the Paketbox)
#   @author     Jannik Erb(jan.erb.21@lehre.mosbach.dhbw.de)
#   @version 1.1
#   @date 2023-03-07

import gpiod
from classes.Config import Config
from classes.Exceptionhandler import ExceptionHandler
from classes.Email import Email
import threading
import time

class ErrButton:
    
    __errorButton = None
    __con = None
    __exception = None
    __email = None
    _instance = None
    ## constructor
    #   @brief Constructor as Singelton <br> init the errorbutton Pin
    #   @param [in] none
    #   @param [out] none
    #   @param [out] _exception    ExceptionHandler
    #   @param [out] _con          Config
    #   @param [out] _email        Email
    #   @param [out] _errorButton  GPIO Error Button
    #   @return      _instance
    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(ErrButton, cls).__new__(cls)

            cls.__exception = ExceptionHandler()
            cls.__con = Config()
            cls.__email = Email()
            ERRORPIN = cls.__con.getConfigAsInt("ErrorButtonPin", 17)    #getting GpioPin from config

            chip = gpiod.chip('gpiochip0')

            cls.__errorButton = chip.get_line(ERRORPIN)

            reqconfig = gpiod.line_request()
            reqconfig.consumer = "errorbutton"
            reqconfig.flag = gpiod.line_request.FLAG_BIAS_PULL_DOWN
            reqconfig.request_type = gpiod.line_request.DIRECTION_INPUT

            cls.__errorButton.request(reqconfig)

        return cls._instance


    ## __getErrButton
    #   @brief  function to read the State of the Button
    #   @param  [in]    none
    #   @param  [out]   none
    #   @return int GPIO.get_value (1 = true, 0 = false)
    def __getErrButton(self):
        return self.__errorButton.get_value()
    
    ## __errButtonThread
    #   @brief  thread for checkin if button is pressed <br>
    #           set Error if button is pressed
    #   @param  [in]    none
    #   @param  [out]   __getErrButton  
    
    def __errButtonThread(self):
        while True:
            if self.__getErrButton():
                self.__exception.warning()
                #self.__email.sendErrorEmail("Error Button wurde gedrückt", )
                time.sleep(1)
            else:
                #nothing
                pass
            time.sleep(0.1)

    ##run
    #   @brief  start thread
    #   @param  [in]    none
    #   @param  [out]   none
    def run(self):
        thread = threading.Thread(target=self.__errButtonThread)
        thread.daemon = True
        thread.start()