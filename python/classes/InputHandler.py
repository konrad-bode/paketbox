#Gruppe32#
##
#   @file InputHandler.py
#   @class InputHandler
#   @brief  class to handle the barcodes
#   @author     Jonas Bodi (jon.bodi.21@lehre.mosbach.dhbw.de)
#   @version 1.1
#   @date 2023-03-07

import os
from classes.Api import Api
from classes.Exceptionhandler import ExceptionHandler
from classes.Locker import Locker
from classes.MqttClient import MqttClient
from classes.Email import Email

class InputHandler:


    __mqttTopic = "runtime/scanner"

    ## constructor
    #   @brief  Constructor 
    #   @param  [in]    none
    #   @param  [out]   none 
    def __init__(self):
        self.__request = Api()
        self.__ex = ExceptionHandler()
        self.__lock = Locker()
        self.__mqttClient = MqttClient()
        self.__mqttSubscribe()
        self.__email = Email()
        __barcode = None

    ## __mqttSubscribe
    #   @brief  function to subscribe to an mqtt topic
    #   @param  [in]    none
    #   @param  [out]   none
    def __mqttSubscribe(self):
        self.__mqttClient.mqttSubscribe(self.__mqttTopic, 0)   #MQTT subscribe to topic: runtime/scanner

    ## Compare
    #   @brief  function to compare the scanned barcode with the expected 
    #   @param  [in]    none
    #   @param  [out]   none
    def compare(self):

        barcode = self.__mqttClient.getdata(self.__mqttTopic, 0, True)   #MQTT wait for message from Scanner
        print(barcode)
        if barcode != None:
            checkedIn = self.__request.put('pakete/accept' , barcode) #compare received Barcode with expected
        else:
            return
        

        if checkedIn == 404:  #when the received barcode and the expected ar not the same, then write log
            self.__ex.log(501)
            self.__email.sendErrorEmail("Barcode nicht erkannt/ stimmt nicht mit erwartetem Barcode überein", 501)
        elif checkedIn == 202: #when the received barcode and the expected are the same, then open the paket box and write log
            self.__lock.open()
            self.__ex.log(200)
            self.__email.sendInfoEmail("Ihr Paket ist da!", 200)