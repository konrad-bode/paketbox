#Gruppe32#
##
#   @class Config
#   @brief  class to connect the config file with needed __values 
#   @warning    by using '#' the hole line will be ignored
#   @author     Jannik Erb (jan.erb.21@lehre.mosbach.dhbw.de)
#   @version 1.5
#   @date 2023-02-16


import os
import time
class Config:
    __scriptpath = None
    __filepath = 'config/config.txt'
    __fileisreading = False
    _instance = None
    __filedata = None
    __rawfield = None
    __argument = None
    __value = None

    ##
    #   @brief Constructor as Singelton <br> read parameter from file
    #   @info   file build:   <argument> = <value>
    #   @info   use '#' to make a comment
    #   @param [in]     none
    #   @param [out]    _scriptpath     Path to Config file
    #   @return   cls._instance     of the object, for Singelton
    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(Config, cls).__new__(cls)
            cls.__scriptpath = os.path.dirname(__file__)
            cls.__readfile(cls) #read file
        else:
            #instance already exists
            pass
        
        return cls._instance


    ##
    #   @brief Destructor not used
    #   @param [in]    none
    #   @param [out]   none
    def __del__(self) -> None:
        pass

    ##
    #   @brief  this methode will remove the char, that is given by the replacement variable <br>
    #           it's used to clear all spaces in the config file
    #   @param  [in]     argument    String, with have to be trimmed
    #   @param  [in]     replacement    char that should be removed
    #   @return         argument   return the argument without the replacement char
    def __removechar(self, __argument, replacement):
        __argument = __argument.replace(replacement, '')
        return __argument


    ##
    #   @brief  checks if the line in the config.txt is defined as a comment section. <br> For comments, see above.
    #   @param  [in]     argument String, with have to be checked of a comment
    #   @return bool     if the argument is a comment it will return True
    def __checkcomment(self, __argument):
        if "#" in __argument:
            return True
        else:
            return False
        
    ##
    #   @brief search the argument field for the given statement 
    #   @param  [in]    SourceArgument   Search the Argument field for this statement 
    #   @return int     return the position of the Element or None if not found
    def ____argumentPos(self, Source__argument):
        i = 0
        for elements in self.__argument:
            if Source__argument in elements:
                return i
            else:
                i+=1

        return None

    ##
    #   @brief  this function read the file and check threw the other function for comments and remove " " chars
    #   @param  [in]    None
    #   @param  [out]   None
    #   @param  __filedata[]  field that contains the raw lines of the file with comments
    #   @param  __rawfield[]  field that contain the raw line of the field without comments
    #   @param  __argument[]  field with all arugments in the file
    #   @param  __value[]     field with all values an the same pos then the argument  
    def __readfile(self):
        self.__fileisreading = True
        i = 0
        self.__filedata = [] #this field contains the raw data with comments 
        self.__rawfield = [] #this field contains the raw lines of the file without comments
        self.__argument = [] #this field contains the Arguments (<argument> = <value>)
        self.__value = [] #this field contains the values
        try:
            path = os.path.join(self.__scriptpath, self.__filepath)
            file = open(path, "r")

            #read file
            for line in file:
                i += 1
                self.__filedata.append(line)
                #check that __argument is not a comment
                if self.__checkcomment(self,line) == False and line != None:
                    line = self.__removechar(self, line, '\n')
                    self.__rawfield.append(line)

            file.close()
        except:
            pass

        #transformation from the raw lines to the outputs
        for elements in self.__rawfield:
            elements = self.__removechar(self, elements, ' ') #remove all spaces
            pos = elements.find('=')

            if pos == -1:
                #fehler (no element find)
                break
            else:                  
                self.__argument.append(elements[0:pos])   #safe the object befor '=' in arguemnt
                self.__value.append(elements[(pos+1): len(elements)]) #safe the object after '=' in __value

        self.__fileisreading = False   


    ##
    #   @brief  writes changes into config file <br> the function will also update the file
    #   @param  [in]    Sourceargument   Argument that should be changed
    #   @param  [in]    oldvalue    oldvalue
    #   @param  [in]    newvalue    newvalue
    #   @warning the file will be updated with the new parameter
    #   @todo for with a counter so now oldValue needed, and problem with dopple values
    def __writefile(self, SourceArgument, oldValue, newValue):

        path = os.path.join(self.__scriptpath, self.__filepath)
        file = open(path, "w")

        #go threw the filek
        for element in self.__filedata:
            element = str(element)

            if element.find(SourceArgument) != -1:
                element = element.replace(oldValue, newValue)
            
            file.write(element)
            

    ##
    #   @brief  Getting Parameter from Config 
    #   @param  [in]    Source_rgument   Get this argument from the config File.
    #   @param  [in]    Defaultargument    Default argument if the config file don't have the argument.
    #   @param  [out]   String  Sourceargument if argument exists or Defaultargument if not.
    #   @info   Will return the value as String
    def getConfigAsString(self, sourceArgument, defaultArgument):
        element = self.____argumentPos(sourceArgument)
        if element != None:

            return str(self.__value[element])
        else:
            #return Default__argument    
            return defaultArgument

    ##
    #   @brief  Getting Parameter from Config 
    #   @param  [in]    sourceArgument   Get this argument from the config File.
    #   @param  [in]    defaultArgument    Default argument if the config file don't have the argument.
    #   @param  [out]   Int  sourceArgument if Argument exists or defaultArgument if not.
    #   @info   Will return the Value as Int
    #   @warning    if it's not an int this will stop the Programm
    def getConfigAsInt(self, sourceArgument, defaultArgument):
        element = self.____argumentPos(sourceArgument)

        if element:
            try:
                intValue = int(self.__value[element])
                return intValue
            except:
                return defaultArgument
        else:
            #return Default__argument    
            return defaultArgument

    ##
    #   @brief  Update Parameter from Config 
    #   @param  [in]    sourceArgument   Get this argument from the config File.
    #   @param  [in]    value   new value that the 
    #   @retrun   String  sourceArgument if argument exists or defaultArgument if not.
    #   @todo   see _writefile
    def updateConfig(self, sourceArgument, value):

        element = self.____argumentPos(sourceArgument)
        if element:
            self.__writefile(sourceArgument, str(self.__value[element]), str(value))
            self.__value[element] = value
            pass
        else:
            #return Default__argument    
            return None