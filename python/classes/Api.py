#Gruppe 32
##
#@class api
#@brief Class to Control Commuinaction to API of the Webserver
#@author Jannik Erb (jan.erb.21@lehre.mosbach.dhbw.de)
#@version 2.0
#@date 2023-02-16

import requests
from classes.Config import Config
from classes.Exceptionhandler import ExceptionHandler
class Api:
    __apiurl = ""
    __timeout = 3
    
    __ex = None

    ##
    #@brief Constructor <br> get config and set API connection
    #@param [in] none
    #@param [out] self.__ex         ExcepionHandler instance
    #@param [out] self.__apiurl     api url address
    def __init__(self) -> None:
        con = Config()
        #if the webserver is running on another IP-Adress
        host = con.getConfigAsString("webHost", "localhost")
        self.__apiurl = "http://"+ host +":8080/api/"
        self.__ex = ExceptionHandler()

    ##
    #   @brief  getting messages from the API
    #   @param  [in]     src     destination of the api
    #   @return    req     Text with the result
    #   @return    None    API is not reachable or value not in db
    #   @info   not for getting a json with multiple elements
    def get(self, src):
        try:
            req = requests.get(self.__apiurl + str(src))
            return req.text #return value from json
        except:
            self.__ex.log(401)
            return None

    ##
    #   @brief  deleting a variable on the db over api
    #   @param  [in]    src         destination of the api
    #   @param  [in]    argument    delete all from argument
    #   @return   status_code Statuscode 202 = ok, 404 = not ok
    #   @return   None        API is not reachable or ardument not in db
    def put(self, src, argument):
        try:
            req = requests.put(self.__apiurl + str(src) + "/" + str(argument))
            self.__ex.log(202)
            return req.status_code
        except:
            self.__ex.log(401)
            return None

    #http post request to api
    ##
    #   @brief  sending data to the api
    #   @param  [in]    src         destination of the api
    #   @param  [in]    data        data that should be transmitted
    #   @return   status_code Statuscode 202 = ok, 404 = not ok
    #   @return   None   API is not reachable or src not found
    #   @info   not for sending a json with multiple elements

    def post(self, src, data):
        try:
            req = requests.post(self.__apiurl + str(src) + "/", data)
            return req.status_code
        except:
            self.__ex.log(401)
            return None

    ##
    #   @brief  sending error message to the api, to show logs on webpage
    #   @param [in]     src         destination of the api, src = logs
    #   @param [in]     data        JSON with failurecode and text of the failure, {text="", code=""}
    #   @param [out]    staus_code  Statuscode 202 = ok, 404 = not ok
    #   @param [out]    None        API is not reachable
    
    def failure(self, src, data):
        try:
            req = requests.post(self.__apiurl + str(src) + "/", data)
            return req.status_code
        except:
            #nothing to log, logging for file in loggerclass
            return None