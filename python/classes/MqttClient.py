#Gruppe32#
##
#   @class Logger
#   @brief  class the MQTT connection
#   @author     Jannik Erb (jan.erb.21@lehre.mosbach.dhbw.de)
#   @version 1.0
#   @date 2023-02-16

#Quellen:
#https://www.eclipse.org/paho/index.php?page=clients/python/docs/index.php#network-loop
#https://docs.oasis-open.org/mqtt/mqtt/v5.0/mqtt-v5.0.html

import paho.mqtt.client as mqtt
import time
import threading
from classes.Config import Config
from classes.Logger import logger

class MqttClient:

    __client = mqtt.Client("", True, None, mqtt.MQTTv311, "tcp")    #not all broker are able to handle MQTTv5
    __arr_subitems = []
    __arr_subitems_qos = []
    __arr_payload = []        
    __arr_topic = []
    __readdelay = 1   #delay for wating of massages
    __connindex = 0
    __readingstate = False
    __threadrunning = False
    __connection = True


    _instance = None
    __ex = None


    #constuctor
    ## constructor
    #   @brief Constructor as Singelton <br> init Door Lock
    #   @param [in]    none
    #   @param [out]   __client         MqttClient
    #   @param [out]   __ex                 ExceptionHandler
    #   @return _instance
    def __new__(cls):
        if cls._instance is None:

            cls._instance = super(MqttClient, cls).__new__(cls)

            con = Config()
            cls.__ex = logger()
            mqttUser = con.getConfigAsString("mqttUser", "py")
            mqttPwd = con.getConfigAsString("mqttPwd", "mosquitto")
            host = con.getConfigAsString("mqttHost", "localhost")
            port = con.getConfigAsInt("mqttPort", 1883)
            keepalive = con.getConfigAsInt("mqttKeepAlive", 10)

            cls.__client.username_pw_set(mqttUser, mqttPwd)
            try:
                cls.__client.connect(host, port, keepalive)   
                cls.__ex.log(203)      
            except:
                #no server alive
                cls.__ex.log(400)
                pass
            finally:
                cls.__client.loop_start()

            
            # Put any initialization here.
        return cls._instance
    

    ##  destructor
    #   @brief  Destructor 
    #   @param  [in]    none
    #   @param  [out]   none
    def __delattr__(self) -> None:
        self.__client.loop_stop(False)
        self.__client.disconnect()

    
        
    ## __thread_reconnect
    #   @brief  thread for reconnecting to MQTT Broker
    #   @param  [in]    none
    #   @param  [out]   none
    def __thread_reconnect(self): 
        self.__threadrunning = True
        i = 0
            
        while not self.__client.is_connected():
            print("test")
            if  i == 0 and self.__connection == True:
                self.__ex.log(312)
                self.__connection = False

            if i <= 100:
                try:
                    self.__client.reconnect()
                except:
                    #thread is started
                    pass            
                i +=1
            else:
                return None

            time.sleep(1)


        if self.__client.is_connected() and not self.__connection:
            print("test2")
            self.__connection = True
            self.__ex.log(203)
            for (sub, qos) in zip(self.__arr_subitems, self.__arr_subitems_qos):
                print(sub)
                print(qos)
                self.mqttSubscribe(sub, qos, 0.5)

        self.__threadrunning = False
        return None
        

    ## __checkconn
    #   @brief  function for checking the connection with the MQTT Broker
    #   @param  [in]    none
    #   @param  [out]   none
    def __checkconn(self):

        try:
            if not self.__threadrunning and not self.__client.is_connected():
                connthread = threading.Thread(target = self.__thread_reconnect)
                connthread.daemon = True
                connthread.start()
        except Exception as ex:
            pass
        

    ## __enablereading
    #   @brief  function that enable reading of the elements
    #   @param  [in]    none
    #   @param  [out]   none
    def __enablereading(self):
        #mehrfaches Aufrufen wird durch paho abgefangen
        self.__client.on_message = self.__onMessage


    ## __messageListdHandler
    #   @brief  function that store the messages and sort this to the subscribes
    #   @param  [in]    message
    #   @param  [in]    topic
    #   @param  [out]   none
    def __messageListdHandler(self, message,topic):

        if topic in self.__arr_topic:
            tmp = self.__arr_topic.index(topic)
            self.__arr_payload[tmp] = str(message)
        else:
            self.__arr_payload.append(message)
            self.__arr_topic.append(topic)  

    ## __onMessage
    #   @brief  function for getting the MQTTMessages from the Broker
    #   @param  [in]    client
    #   @param  [in]    userdata
    #   @param  [in]    messae
    #   @param  [out]   none
    def __onMessage(self, client, userdata, message):
        payload = message.payload.decode("utf-8")
        topic = message.topic
        retain = message.retain
        if not retain:
            self.__messageListdHandler(payload, topic)
        else:
            #ignore
            pass
        

    ## mqttPublish
    #   @brief  function for publishing to the MQTT Broker
    #   @param  [in]    topic
    #   @param  [in]    message
    #   @param  [in]    qos
    #   @param  [out]   none
    def mqttPublish(self, topic, message, qos):
        self.__checkconn()
        #the mosquitto broker don't support qos 2 so all qos > 1 will set to 1
        if qos > 1:
            self.__ex.log(310)
            qos = 1
        elif qos < 0:
            self.__ex.log(310)
            qos = 0
        else:
            pass

        self.__client.publish(topic, message, qos)
            

    ## mqttPublish
    #   @brief  function for subscribing MQTT Topics
    #   @param  [in]    topic
    #   @param  [in]    qos
    #   @param  [in]    delaytime
    #   @param  [out]   none
    #   @info   low cpu's need a little delay befor subscribing after connecting to the Broker
    def mqttSubscribe(self, topic, qos, delaytime=0.1):
        time.sleep(delaytime)
        self.__checkconn()
        self.__client.subscribe(topic, qos)

        self.__enablereading()

        #item only append if not existing
        if  topic not in self.__arr_subitems:
            self.__arr_subitems.append(topic)
            self.__arr_subitems_qos.append(qos)
            return True
        else:
            return False
        

    ## mqttPublish
    #   @brief  function for unsubscribing MQTT Topics
    #   @param  [in]    topic
    #   @param  [out]   none
    def mqttUnsubscribe(self,topic):
        self.__checkconn()
        self.__client.unsubscribe(topic)

        if  topic in self.__arr_subitems:
            topic_pos = self.__arr_subitems.index(topic)    #get position auf the topic item for qos
            self.__arr_subitems.pop(topic)
            self.__arr_subitems.pop(topic_pos)
            return True
        else:
            self.__ex.log(311)
            #no such item
            return False
        
    
    ## mqttPublish
    #   @brief  function for getting the Last Message as an Array
    #   @param  [in]    readdelay
    #   @param  [out]   none
    #   return  __arr_payload, __arr_topic      tupel
    #   @info   low cpu's need a little delay befor gettingData after connecting to the Broker
    def getrawdata(self, readdelay : 0):
        self.__checkconn()
        time.sleep(readdelay)  #delay reading usefull if above is a publish message
        return self.__arr_payload, self.__arr_topic


    # mqttPublish
    #   @brief  function for getting the Last Message as an Array
    #   @param  [in]    readdelay
    #   @param  [in]    setnull     Delete Message
    #   @param  [out]   none
    #   return  msg     String or None if not existing
    #   @info   low cpu's need a little delay befor gettingData after connecting to the Broker
    def getdata(self, topic, readdelay : 0, setnull  : False):
        self.__checkconn()
        time.sleep(readdelay)  #delay reading usefull if above is a publish message
        if topic in self.__arr_topic:
            tmp = self.__arr_topic.index(topic)
            msg = self.__arr_payload[tmp]
            #if setnull is anabled the message will set to None if it was read
            if setnull:
                self.__arr_payload[tmp] = None
            
            return msg
        else:
            return None