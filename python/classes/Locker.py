#Gruppe32#
##
#   @file Locker.py
#   @class Locker
#   @brief  class to control the lock
#   @author     Jonas Bodi (jon.bodi.21@lehre.mosbach.dhbw.de) <br>
#               Jannik Erb (jan.erb.21@lehre.mosbach.dhbw.de)
#   @version 1.1
#   @date 2023-03-07

import time
import gpiod
from classes.Config import Config
from classes.MqttClient import MqttClient
import threading

        
class Locker:

    __mqttClient =  None
    __mqttTopic = "runtime/io/rm"
    __ioOn = '{"function": "lock", "value": true}'  #open
    __ioOff = '{"function": "lock", "value": false}'    #closed
    __ioState = False
    __threadrunning = False
    __con = None
    __signal = None
    __lock = None


    _instance = None
    ##  constructor
    #   @brief  Constructor as Singelton 
    #   @param  [in]    none
    #   @param  [out]   __con           Config
    #   @param  [out]   __mqttClient    MqttClient
    #   @param  [out]   __isClosed      True
    #   @param  [out]   __lock          GPIO, Lock
    #   @param  [out]   __signal        GPIO, Signal
    #   @return   _instance
    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(Locker, cls).__new__(cls)
            cls.__con = Config()
            cls.__mqttClient = MqttClient()

            cls.__isClosed = True   #variable used to control whether the box is closed or opened
        
            LOCKPIN = cls.__con.getConfigAsInt("LockerPin", 2)    #gets pin number for the locker from config
            SIGNALPIN = cls.__con.getConfigAsInt("SignalPin", 3)   #gets pin number for the signal led from config
                
            chip = gpiod.chip('gpiochip0')  #connecting to chip 0

            cls.__lock = chip.get_line(LOCKPIN)   #Pin gets named
            cls.__signal = chip.get_line(SIGNALPIN)
            
            #config for lockerpin
            lockconfig = gpiod.line_request()
            lockconfig.consumer = "lock"
            lockconfig.flag = gpiod.line_request.FLAG_ACTIVE_LOW
            lockconfig.request_type = gpiod.line_request.DIRECTION_OUTPUT
            cls.__lock.request(lockconfig)

            #config for signalpin
            signalconfig = gpiod.line_request()
            signalconfig.consumer = "signal"
            signalconfig.flag = gpiod.line_request.FLAG_ACTIVE_LOW
            signalconfig.request_type = gpiod.line_request.DIRECTION_OUTPUT
            cls.__signal.request(signalconfig)

        return cls._instance
        
    ##  __openthread
    #   @brief  function to open the locker for 5 seconds as thread
    #   @param  [in]    none
    #   @param  [out]   __ioState           switch State
    #   @param  [out]   __threadrunning     set to True while running

    def __openthread(self): #calls open method from Locker, waits for 5 seconds and calls close method from Locker
        self.__threadrunning = True
        self.__openLock()
        self.__ioState = True
        self.__mqttClient.mqttPublish(self.__mqttTopic, self.__ioOn, 0)
        time.sleep(5)
        self.__closeLock()
        self.__ioState = False
        self.__mqttClient.mqttPublish(self.__mqttTopic, self.__ioOff, 0)
        self.__threadrunning = False

    ##  __openLock
    #   @brief  function to open the lock
    #   @param  [in]    none
    #   @param  [out]   __ioState   False 
    def __openLock(self): #opens locker, turns signal led on and sets control variable on False
        self.__isClosed = False
        self.__lock.set_value(1)
        self.__signal.set_value(1)


    ##  __closeLock
    #   @brief  function to close the lock
    #   @param  [in]    none
    #   @param  [out]   __ioState   True  
    def __closeLock(self):    #closes locker, turns signal led off and sets control variable on True
        self.__isClosed = True
        self.__lock.set_value(0)
        self.__signal.set_value(0)

    ##  open
    #   @brief  function to start the thread
    #   @param  [in]    none
    #   @param  [out]   none    
    def open(self):  #calls openthread 
        
        #only start thread if not used
        if not self.__threadrunning:
            thread = threading.Thread(target = self.__openthread)
            thread.daemon = True
            thread.start()
        else:
            #idle
            pass

    ##  getioState
    #   @brief  function to transfer the status to the webserver
    #   @param  [in]    none
    #   @param  [out]   none    
    def getioState(self):   #sends io state to webserver
        if self.__ioState:

            self.__mqttClient.mqttPublish(self.__mqttTopic, self.__ioOn, 0)

        else:
            self.__mqttClient.mqttPublish(self.__mqttTopic, self.__ioOff, 0)
    