#Gruppe32#
##
#   @class Light
#   @brief  class for control of the Light
#   @author     Jonas Bodi (jon.bodi.21@lehre.mosbach.dhbw.de) <br>
#               Jannik Erb (jan.erb.21@lehre.mosbach.dhbw.de)
#   @version 1.1
#   @date 2023-03-07

import time
import gpiod
from classes.Config import Config
from classes.MqttClient import MqttClient
import threading


class Light:

    __led = None
    __force = False    #force value that if light is manually on the automatic mode is diabled
    __mqttClient =  None
    __mqttTopic = "runtime/io/rm"
    __ioOn = '{"function": "light", "value": true}'
    __ioOff = '{"function": "light", "value": false}'
    __ioState = False
    _ontime = 0
    __threadrunnning = False
    
    _instance = None

    ## constructor
    #   @brief  Constructor as Singelton <br> init the errorled Pin
    #   @param  [in]    none
    #   @param [out]    _exception    ExceptionHandler
    #   @param [out]    _con          Config
    #   @param [out]    _led          GPIO path Light
    #   @return         _instance 
    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(Light, cls).__new__(cls)

            cls.__con = Config()
    
            __LEDPIN = cls.__con.getConfigAsInt("LightPin", 4)
            cls.__mqttClient = MqttClient()
            chip = gpiod.chip('gpiochip0')

            cls.__led = chip.get_line(__LEDPIN)

            reqconfig = gpiod.line_request()
            reqconfig.consumer = "light"
            reqconfig.flag = gpiod.line_request.FLAG_ACTIVE_LOW
            reqconfig.request_type = gpiod.line_request.DIRECTION_OUTPUT

            cls.__led.request(reqconfig)

            cls.__ontime = cls.__con.getConfigAsInt("LedTime", 60)
        
        return cls._instance

    ##  __onthread
    #   @brief  function to turn the light for one minute on as thread
    #   @param  [in]    none
    #   @param  [out]   none
    def __onthread(self):
        self.__threadrunnning = True
        if not self.__force:
            
            self.__led.set_value(1)
            self.__ioState = True
            self.__mqttClient.mqttPublish(self.__mqttTopic, self.__ioOn, 0)
            time.sleep(self.__ontime)
            self.__led.set_value(0)
            self.__ioState = False
            self.__mqttClient.mqttPublish(self.__mqttTopic, self.__ioOff, 0)

        self.__threadrunnning = False

    ##  automaticOn
    #   @brief  function to turn the thread on
    #   @param  [in]    none
    #   @param  [out]   none
    def automaticOn(self):
        if not self.__threadrunnning:
            thread = threading.Thread(target = self.__onthread)
            thread.daemon = True
            thread.start()
        else:
            #idle
            pass

    ##  manuallyOn
    #   @brief  function to turn the light manually on
    #   @param  [in]    none
    #   @param  [out]   none   
    def manuallyOn(self):
        self.__force = True
        self.__led.set_value(1)
        self.__ioState = True
        self.__mqttClient.mqttPublish(self.__mqttTopic, self.__ioOn, 0)  

    ##  manuallyOff
    #   @brief  function to turn the light manually off
    #   @param  [in]    none
    #   @param  [out]   none
    def manuallyOff(self):
        self.__force = False
        self.__led.set_value(0)
        self.__ioState = False
        self.__mqttClient.mqttPublish(self.__mqttTopic, self.__ioOff, 0)
      
    ##  getioState
    #   @brief  function to transfer the status to the webserver
    #   @param  [in]    none
    #   @param  [out]   none    
    def getioState(self):
        if self.__ioState:

            self.__mqttClient.mqttPublish(self.__mqttTopic, self.__ioOn, 0)

        else:
            self.__mqttClient.mqttPublish(self.__mqttTopic, self.__ioOff, 0)