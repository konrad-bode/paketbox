#Gruppe32#
##
#   @file Exceptionhandler.py
#   @class ExceptionHandler
#   @brief  class to react to errors and to log
#   @author     Jonas Bodi (jon.bodi.21@lehre.mosbach.dhbw.de)
#   @version 1.1
#   @date 2023-03-07


from classes.Error_Handler import ErrorHandler
from datetime import datetime
from classes.Logger import logger
from classes.Cloud import Cloud


class ExceptionHandler:

    _instance = None
    __errorhandler = None
    __i = None
    __log = None


    ##  constructor
    #   @brief  Constructor as Singelton 
    #   @param  [in]    none
    #   @param  [out]   __log   logger
    #   @param  [out]   __errorhandler   ErrorHandler
    #   @param  [out]   __cloud          Clour
    #   @return   _instance
    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(ExceptionHandler, cls).__new__(cls)
            cls.__i = True
            cls.__log = logger()
            cls.__errorhandler = ErrorHandler()
            cls.__cloud = Cloud()
            
        return cls._instance

    ##  destructor
    #   @brief  Destructor 
    #   @param  [in]    none
    #   @param  [out]   none
    def __delattr__(self, __name: str) -> None:
        pass

    ##  log
    #   @brief  function to log and decide if it is an error or a warning
    #   @param  [in]    code
    #   @param  [out]   none
    def log(self, code):
        time = str(datetime.today().strftime("%d/%m/%Y - %H:%M:%S"))
        try:
            self.__log.log(code)
            if 400 <= code < 600:
                self.Error()
            elif 300 <= code <400:
                self.Warning()
                
        except:
            pass

    ##  Error
    #   @brief  function to stop the system and turn the errorLed on
    #   @param  [in]    none
    #   @param  [out]   none
    def error(self):
        self.__i = False
        self.__errorhandler.errorLedOn()
        self.__cloud.push(False)
        

    ##  ResetError
    #   @brief  function to set teh system back to run and turn the errorLed off
    #   @param  [in]    none
    #   @param  [out]   none
    def resetError(self):
        self.__i = True
        self.__errorhandler.errorLedOff()
        self.__cloud.push(True)
        
    ##  Warning
    #   @brief  function to turn the errorLed on
    #   @param  [in]    none
    #   @param  [out]   none
    def warning(self):
        self.__errorhandler.warningLedOn()

    ##  ResetWarning
    #   @brief  function to turn the errorLed off
    #   @param  [in]    none
    #   @param  [out]   none
    def wesetWarning(self):
        self.__errorhandler.warningLedOff()

  

    ##  Check
    #   @brief  function to check the status of the runtime variable
    #   @param  [in]    none
    #   @param  [out]   none
    def Check(self):
        return self.__i


   
        


    
