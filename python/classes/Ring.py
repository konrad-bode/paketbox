#Gruppe32#
##
#   @file Locker.py
#   @class Locker
#   @brief  class to control the lock
#   @author     Jannik Erb (jan.erb.21@lehre.mosbach.dhbw.de)
#   @version 1.1
#   @date 2023-03-07

import time
import gpiod
from classes.Config import Config
from classes.MqttClient import MqttClient
import threading

        
class Ring:

    __ioState = False
    __threadrunning = False
    __statethreadrunning = False
    __con = None
    __ringIo = None
    __ringAlive = False
    __mqttTopic = "runtime/io/rm"
    __ioAlive = '{"function": "ringAlive", "value": true}'    #closed
    __ioDead = '{"function": "ringAlive", "value": false}'    #closed
    __stateTopic = "esp/outside"

    _instance = None
    ## constructor
    #   @brief  Constructor as Singelton <br> init the errorled Pin
    #   @param  [in]    none
    #   @param  [out]   __con           Config
    #   @param  [out]   __mqttClient    MqttClient
    #   @param  [out]   __isClosed      True
    #   @param  [out]   __ringIo          GPIO, Klingel
    #   @return   _instance
    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(Ring, cls).__new__(cls)
            cls.__con = Config()
            cls.__mqttClient = MqttClient()

            cls.__isClosed = True   #variable used to control whether the box is closed or opened
        
            SIGNALPIN = cls.__con.getConfigAsInt("DoorRingPin", 21)   #gets pin number for the signal led from config
                
            chip = gpiod.chip('gpiochip0')  #connecting to chip 0
            cls.__ringIo = chip.get_line(SIGNALPIN)

            #config for signalpin
            signalconfig = gpiod.line_request()
            signalconfig.consumer = "signal"
            signalconfig.flag = gpiod.line_request.FLAG_ACTIVE_LOW
            signalconfig.request_type = gpiod.line_request.DIRECTION_OUTPUT
            cls.__ringIo.request(signalconfig)

        return cls._instance
        
    ##  __openthread
    #   @brief  function to open the locker for 5 seconds as thread
    #   @param  [in]    none
    #   @param  [out]   __threadrunning     is set while thread is running
    def __openthread(self): #calls open method from Locker, waits for 5 seconds and calls close method from Locker
        self.__threadrunning = True
        self.__ringIo.set_value(1)
        time.sleep(5)
        self.__ringIo.set_value(0)
        self.__threadrunning = False


    ##  ring
    #   @brief  function to start the thread to Ring
    #   @param  [in]    none
    #   @param  [out]   none    
    def ring(self):  #calls openthread 
        #only start thread if not used
        if not self.__threadrunning:
            thread = threading.Thread(target = self.__openthread)
            thread.daemon = True
            thread.start()
        else:
            #idle
            pass

    ##  __ringStateThread
    #   @brief  function to activate the Ring
    #   @param  [in]    none
    #   @param  [out]   __statethreadrunning    is set while thread is running
    #   @param  [out]   __ringAlive             Set if Ring is enabled

    def __ringStateThread(self):
        self.__statethreadrunning = True
        self.__mqttClient.mqttSubscribe(self.__stateTopic, 0, 0)
        while True:
            msg = self.__mqttClient.getdata(self.__stateTopic, 0, True)
            if msg != None:
                self.__ringAlive = True
                self.__mqttClient.mqttPublish(self.__mqttTopic, self.__ioAlive, 0)
            else:
                self.__ringAlive = False
                self.__mqttClient.mqttPublish(self.__mqttTopic, self.__ioDead, 0)
                pass

            time.sleep(60)
        self.__statethreadrunning = False


    ##  getioState
    #   @brief  function to transfer the status to the webserver
    #   @param  [in]    none
    #   @param  [out]   none    
    def getringState(self):
        if self.__ringAlive:
            self.__mqttClient.mqttPublish(self.__mqttTopic, self.__ioAlive, 0)
        else:
            self.__mqttClient.mqttPublish(self.__mqttTopic, self.__ioDead, 0)

    ##  __ringStateThread
    #   @brief  function to activate the __ringStateThread to acitvate the Ring controll
    #   @param  [in]    none
    #   @param  [out]   none

    def runRingState(self):
        if not self.__statethreadrunning:
            thread = threading.Thread(target = self.__ringStateThread)
            thread.daemon = True
            thread.start()
        else:
            #idle
            pass

    