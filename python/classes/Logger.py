#Gruppe32#
##
#   @file Locker.py
#   @class Locker
#   @brief  class to control the lock
#   @author     Konrad Bode (kon.bode@lehre.mosbach.dhbw.de)
#   @version 1.1
#   @date 2023-03-07


import logging
import logging.config
import logging.handlers
import os

class logger():
    
    __statusCodes = {
        200: "Barcode erkannt",
        201: "Email verschickt",
        202: "Verbindung mit Api",
        203: "mqttClient initialisiert",
        204: "mqttClient verbunden",
        205: "verbunden mit smtp",
        
        300: "Keine SMTP Mail hinterlegt",
        310: "QoS ausserhalb Reichweite",
        311: "Fehler beim Unsubscriben vom MQTT Item",
        312: "Verbindung zu MQTT verloren",

        400: "MQTT Initialisierungs-Fehler",
        401: "API Verbindungs-Fehler",
        402: "SMTP Verbindungs-Fehler",
        410: "Falscher Variablen-Typ",
        411: "",
        420: "Fehler beim Loggen",

        500: "Broker Initialisierung fehlgeschlagen",
        501: "Barcode nicht erkannt",

        600: "Zulieferung des Paketes nicht möglich"
    }
    
    def __init__(self):
        scriptpath = os.path.dirname(__file__)
        filepath = "logging.conf"
        path = os.path.join(scriptpath, filepath)

        logging.config.fileConfig(path)
        self.logger = logging.getLogger('logger')
        
    def log(self,code):
        text = str(code) + " - " + self.__statusCodes[code]
        if(code >= 200 and code < 300):
            self.logger.info(text)
        elif(code >= 300 and code < 400):
            self.logger.warn(text)
        else:
            self.logger.error(text)