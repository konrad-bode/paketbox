#Gruppe32#
##
#   @class Cloud
#   @brief  Class for Handle the Connection to the Thingspeak Cloud
#   @author     Konrad Bode(kon.bode@lehre.mosbach.dhbw.de)
#   @version 1.0
#   @date 2023-03-11

import requests
from classes.Config import Config

class Cloud():
    __url = None
    def __init__(self):
        conf = Config()
        apiKey = conf.getConfigAsString("CloudApiKey", None)
        self.__url = 'https://api.thingspeak.com/update?api_key=%s&field0='%apiKey
        
    def push(self,run):
        try:
            request(self.__url + str(run))
        except:
            pass