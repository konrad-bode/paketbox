#Gruppe32#
##
#   @file Error_Handler.py
#   @class ErrorHandler
#   @brief  class to control the ErrorLed and communicate with the webserver
#   @author     Jonas Bodi (jon.bodi.21@lehre.mosbach.dhbw.de)
#               Jannik Erb (jan.erb.21@lehre.mosbach.dhbw.de)
#   @version 1.1
#   @date 2023-03-07

import gpiod
from classes.Config import Config
from classes.MqttClient import MqttClient

class ErrorHandler:
    
    __errorLed = None
    __mqttClient = None
    __mqttTopic = "runtime/io/rm"
    __fRun = '{"function": "function", "value": "run"}'
    __fWarn = '{"function": "function", "value": "warning"}'
    __fStop = '{"function": "function", "value": "stop"}'
    __con = None
    __error = None
    __warn = True


    _instance = None

    ## constructor
    #   @brief  Constructor as Singelton <br> init the errorled Pin
    #   @param  [in]    none
    #   @param  [out]   __mqttClient    MqttClient
    #   @param  [out]   __con           Config
    #   @param  [out]   __errorLed         GPIO Error Led
    #   @param  [out]   __error             Error State = True (run)
    #   @return   _instance
    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(ErrorHandler, cls).__new__(cls)
            cls.__error = True
            cls.__con = Config()
            cls.__mqttClient = MqttClient()
            ERRORPIN = cls.__con.getConfigAsInt("ErrorPin", 26)    #getting GpioPin from config
            chip = gpiod.chip('gpiochip0')

            cls.__errorLed = chip.get_line(ERRORPIN)

            reqconfig = gpiod.line_request()
            reqconfig.consumer = "errorled"
            reqconfig.flag = gpiod.line_request.FLAG_ACTIVE_LOW
            reqconfig.request_type = gpiod.line_request.DIRECTION_OUTPUT
            cls.__errorLed.request(reqconfig)
        
        return cls._instance

    ## ErrorLedOn
    #   @brief  function to turn the ErrorLed on
    #   @param  [in]    none
    #   @param  [out]   __error             Error State = False (stop)
    def errorLedOn(self):
        self.__errorLed.set_value(1)
        self.__error = False
        self.__warn = True
        self.__mqttClient.mqttPublish(self.__mqttTopic, self.__fStop, 0)

    ## ErrorLedOff
    #   @brief  function to turn the ErrorLed off
    #   @param  [in]    none
    #   @param  [out]   __error             Error State = True (run)
    def errorLedOff(self):
        self.__errorLed.set_value(0)
        self.__error = True
        self.__warn = True
        self.__mqttClient.mqttPublish(self.__mqttTopic, self.__fRun, 0)

    ## WarningLedOn
    #   @brief  function to turn the WarningLed on
    #   @param  [in]    none
    #   @param  [out]   none
    def warningLedOn(self):
        self.__errorLed.set_value(1)
        self.__warn = False
        self.__mqttClient.mqttPublish(self.__mqttTopic, self.__fWarn, 0)

    ## WarningLedOff
    #   @brief  function to turn the WarningLed off
    #   @param  [in]    none
    #   @param  [out]   none
    def warningLedOff(self):
        self.__errorLed.set_value(0)
        self.__warn = False
        self.__mqttClient.mqttPublish(self.__mqttTopic, self.__fRun, 0)

    ## GetFunction
    #   @brief  function to transfer the status to the webserver
    #   @param  [in]    none
    #   @param  [out]   none
    def getFunction(self):
        if self.__error:
            pass
            self.__mqttClient.mqttPublish(self.__mqttTopic, self.__fRun, 0)
        else:
            pass
            self.__mqttClient.mqttPublish(self.__mqttTopic, self.__fStop, 0)
        
        if not self.__warn:
            self.__mqttClient.mqttPublish(self.__mqttTopic, self.__fWarn, 0)