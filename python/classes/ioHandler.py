#Gruppe 32
##
#@class ioHandler
#@brief Class to Controll all the IO's of the Raspberry PI
#@author Jannik Erb (jan.erb.21@lehre.mosbach.dhbw.de)
#@version 1.0
#@date 2023-02-16

from classes.Light import Light
from classes.Locker import Locker
from classes.UseTime import UseTime
from classes.MqttClient import MqttClient
from classes.Exceptionhandler import ExceptionHandler
from classes.Error_Handler import ErrorHandler
from classes.ErrButton import ErrButton
from classes.DoorButton import DoorButton
from classes.DoorLock import DoorLock
from classes.Ring import Ring
import threading
import time
import json

class ioHandler:
    
    __light = None
    __locker = None
    __mqttClient = None
    __exception = None
    __error = None
    __useTime = None
    __errButton = None
    __doorButton = None
    __doorLock = None
    __doorRing = None

    __mqttTopicFunction = "runtime/io/function"
    __mqttTopicGet = "runtime/io/get"
    __mqttTopicTime = "runtime/io/time"
    __mqttTopicAction = "runtime/io/action"
    __mqttTopicEspRing = "esp/ring"


    ## constructor
    #   @brief Constructor as Singelton <br> init Door Lock
    #   @param [in]    none
    #   @param [out]   __mqttClient         MqttClient
    #   @param [out]   __light              Light
    #   @param [out]   __locker             Locker
    #   @param [out]   __useTime            UseTime
    #   @param [out]   __error              ErrorHandler
    #   @param [out]   __errButton          ErrButton
    #   @param [out]   __doorLock           DoorLock
    #   @param [out]   __doorButton        DoorButton
    #   @param [out]   __doorRing          Ring
    #   @return _instance
    def __init__(self) -> None:
        self.__mqttClient = MqttClient()
        self.__light = Light()
        self.__locker = Locker()
        self.__useTime = UseTime()
        self.__exception = ExceptionHandler()
        self.__error = ErrorHandler()
        self.__errButton = ErrButton()
        self.__doorLock = DoorLock()
        self.__doorButton = DoorButton()
        self.__doorRing = Ring()
        self.__mqttSubscribe()

    
    ## __mqttSubscribe
    #   @brief  function to Subscribe to all needed topics
    #   @param  [in]    none
    #   @param  [out]   none
    def __mqttSubscribe(self):
        self.__mqttClient.mqttSubscribe(self.__mqttTopicAction, 0)
        self.__mqttClient.mqttSubscribe(self.__mqttTopicTime, 0)
        self.__mqttClient.mqttSubscribe(self.__mqttTopicGet, 0)
        self.__mqttClient.mqttSubscribe(self.__mqttTopicFunction, 0)
        self.__mqttClient.mqttSubscribe(self.__mqttTopicEspRing, 0)

#area : mqttMessages

    ## __mqttFunctionMessage
    #   @brief  function to get Message from Mqtt
    #   @param  [in]    none
    #   @param  [out]   none
    #   @return     Message from MQTT Topic __mqttTopicFuction if arrived
    def __mqttFunctionMessage(self):
        return self.__mqttClient.getdata(self.__mqttTopicFunction, 0, True)

    ## __mqttGetMessage
    #   @brief  function to get Message from Mqtt
    #   @param  [in]    none
    #   @param  [out]   none
    #   @return     Message from MQTT Topic __mqttTopicGet if arrived
    def __mqttGetMessge(self):
        return self.__mqttClient.getdata(self.__mqttTopicGet, 0, True)

    ## __mqttManMessage
    #   @brief  function to get Message from Mqtt
    #   @param  [in]    none
    #   @param  [out]   none
    #   @return     Message from MQTT Topic __mqttTopicAction if arrived
    def __mqttManMessage(self):
        return self.__mqttClient.getdata(self.__mqttTopicAction, 0, True)

    ## __mqttAutoMessage
    #   @brief  function to get Message from Mqtt
    #   @param  [in]    none
    #   @param  [out]   none
    #   @return     Message from MQTT Topic __mqttTopicEspRing if arrived
    def __mqttAutoMessage(self):
        return self.__mqttClient.getdata(self.__mqttTopicEspRing, 0, True)

    ## __mqttTimeMessage
    #   @brief  function to get Message from Mqtt
    #   @param  [in]    none
    #   @param  [out]   none
    #   @return     Message from MQTT Topic __mqttTopicTime if arrived
    def __mqttTimeMessage(self):
        return self.__mqttClient.getdata(self.__mqttTopicTime, 0, True)
#endarea

#area : handle With IO

    ## __handleAutoLight
    #   @brief  function to handle the AutoLight function path lighting
    #   @param  [in]    state
    #   @param  [out]   none
    def __handleAutoLight(self, state):
        if state:
            self.__light.automaticOn()
            self.__doorRing.ring()
        else:
            #idle
            pass

    ## __handleManLight
    #   @brief  function to handle the ManLight function path lighting
    #   @param  [in]    state
    #   @param  [out]   none
    def __handleManLight(self, state):
        if state:
            self.__light.manuallyOn()
        else:
            self.__light.manuallyOff()

    ## __handleLock
    #   @brief  function to open the Lock of the Paketbox
    #   @param  [in]    state
    #   @param  [out]   none
    def __handleLock(self, state):
        if state:
            self.__locker.open()
        else:
            #the lock openes for 10 sec, not closing
            pass

    ## __handleFunction
    #   @brief  function to reset the Error and Warning from the Webpage
    #   @param  [in]    state
    #   @param  [out]   none
    def __handleFunction(self):
        self.__exception.resetError()

    ## __handleDoorLockEnable
    #   @brief  function to Enable and Disable the DoorLock
    #   @param  [in]    state
    #   @param  [out]   none
    def __handleDoorLockEnable(self, state):
        if state:
            self.__doorLock.enable()
        else:
            self.__doorLock.disable()

    ## __handletime
    #   @brief  function to change the Use Time of the boy
    #   @param  [in]    timeOn
    #   @param  [in]    timeOff
    #   @param  [out]   none
    def __handleTime(self, timeOn, timeOff):
        self.__useTime.setTime(timeOn, timeOff)


#endarea
    ## __GetAllRM
    #   @brief  function for reading alle IO States and sending to WebClient
    #   @param  [in]    none
    #   @param  [out]   none
    def __getAllRM(self):
        self.__light.getioState()
        self.__locker.getioState()
        self.__useTime.getTime()
        self.__error.getFunction()
        self.__doorLock.getenableState()
        self.__doorRing.getringState()


    ## __handleMessage
    #   @brief  function to distribute the message to the handle functions
    #   @param  [in]    message
    #   @param  [out]   none
    def __handleMessage(self, message):
        try:
            message = json.loads(message)
        except:
            return

        if message['function'] == "light":
            self.__handleManLight(message['value'])
        
        elif message['function'] == "lock":
            self.__handleLock(message['value'])

        elif message['function'] == "autolight":
            self.__handleAutoLight(message['value'])
            
        elif message['function'] == "time":
            startTime = message['value']['start']
            stopTime = message['value']['stop']
            self.__handleTime(startTime, stopTime)

        elif message['function'] == "function":
            self.__handleFunction()
        
        elif message['function'] == "doorEnable":
            self.__handleDoorLockEnable(message['value'])

        else:
            #log
            pass

#area : run functions

    ## __runthread
    #   @brief  thread to handle the communication with the webClient in the back
    #   @param  [in]    none
    #   @param  [out]   none
    def __runthread(self):
        while True:
            #check for values
            manValue = self.__mqttManMessage()      #io from webpage
            autoValue = self.__mqttAutoMessage()    #io from esp
            timeValue = self.__mqttTimeMessage()    #time from webpage
            functionValue = self.__mqttFunctionMessage()
            getValue = self.__mqttGetMessge()       #sending akt value to webpage

            #check if true
            if manValue :
                self.__handleMessage(manValue)
            
            if autoValue:
                self.__handleMessage(autoValue)
         
            if getValue:
                self.__getAllRM()

            if timeValue:
                self.__handleMessage(timeValue)

            if functionValue:
                self.__handleMessage(functionValue)

            #performance reason
            time.sleep(0.5)


    ## runasthread
    #   @brief  funciton for starting the thread
    #   @param  [in]    none
    #   @param  [out]   none
    def runasthread(self):
        self.__getAllRM()
        self.__errButton.run()
        self.__doorButton.run()
        self.__doorRing.runRingState()
        thread = threading.Thread(target = self.__runthread)
        thread.daemon = True
        thread.start()
#endarea