#!/usr/bin/env python3

#Gruppe32#
##
#   @file main.py
#   @brief  main
#   @author     Jonas Bodi (jon.bodi.21@lehre.mosbach.dhbw.de)
#   @version 1.1
#   @date 2023-03-07

import time
from classes.ioHandler import ioHandler
from classes.UseTime import UseTime
from classes.ErrButton import ErrButton
from classes.InputHandler import InputHandler
from classes.UseTime import UseTime
from classes.Email import Email


def setup():
    handler = ioHandler()
    handler.runasthread()


def loop(inHandler, email):
    email.infoMailCheck()
    inHandler.compare()
    time.sleep(0.1)
    pass


def idle():
    time.sleep(1)






def main():
    email = Email()
    inHandler = InputHandler()
    useTime = UseTime()
    setup()

    while(True):
        
        if(useTime.check()):
            loop(inHandler, email)
        else:
            idle()


if __name__ == "__main__":
    main()